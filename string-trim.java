// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution {
    public String solution(String message, int K) {
        // write your code in Java SE 8

    if(message.length() <= K) {
    return message;
}
    if(message.charAt(K) == ' ') {
    return message.substring(0,K).trim();
}
    for(int i=K-1; i>=0; i--) {
        if(message.charAt(i) == ' ') {
        return message.substring(0, i+1).trim();
    }
}
    return "";

    }
}
